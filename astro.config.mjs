import { defineConfig } from 'astro/config';

import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  sitemap: true,
  site: "https://dutchtutor.org/",
  integrations: [tailwind()],
  outDir: "public",
  publicDir: "publicAssets"
});